import json

from webcacheclient import WebCacheClient


class TestClass:
    def setup(self):
        self.client = WebCacheClient()

    def test_get_request_credit(self):
        url_list = [
            f"https://nominatim.openstreetmap.org/search/Credit Suisse,{zipcode},Switzerland?osm_type=N&format=json"
            for zipcode in range(2095, 2099)]
        ret = self.client.fetch_urls(url_list, category="OSM-geocoding", output="json")
        assert not any(obj.get('error') for obj in ret.values())

    def test_get_request_bank(self):
        url_list = [
            f"https://nominatim.openstreetmap.org/search/Bank,{zipcode},Switzerland?osm_type=N&format=json"
            for zipcode in range(2095, 2199)]
        ret = self.client.fetch_urls(url_list, category="OSM-geocoding", output="json")
        assert not any(obj.get('error') for obj in ret.values())

    def test_get_request_without_proxies(self):
        url_list = ["http://localhost:7070/search/Switzerland?osm_type=N&format=json",
                    "http://localhost:7070/search/Germany?osm_type=N&format=json"]
        ret = self.client.fetch_urls(url_list, category="OSM-geocoding", output="json")
        assert not any(obj.get('error') for obj in ret.values())

    def test_post_request(self):
        url_list = [f"https://search.wdoms.org?sSchoolName={medschool}&iPageNumber=1" for medschool in
                    ['Basel', 'Zürich', 'London']]
        ret = self.client.fetch_urls(url_list, category="medschool", output="xml", method="POST")
        assert not any(obj.get('error') for obj in ret.values())

    def test_post_data_request(self):
        url_list = [("https://search.wdoms.org", json.dumps({'sSchoolName': medschool, 'iPageNumber': 1}))
                    for medschool in ['Basel', 'Zürich', 'London']]
        ret = self.client.fetch_urls(url_list, category="medschool", output="xml", method="POST")
        assert not any(obj.get('error') for obj in ret.values())

    def test_get_proxy(self):
        res = self.client.get_proxy_list(10000)
        assert res
