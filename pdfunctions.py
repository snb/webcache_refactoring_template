import datetime


class SkipURL(Exception):
    pass


def time_diff_to_now(dt):
    diff = (datetime.datetime.now() - dt)
    return diff.microseconds + diff.seconds * 10 ** 6
