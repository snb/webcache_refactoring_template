import base64
import bz2
import json
import logging
import os
import pickle
from os.path import expanduser

import furl
import requests

import settings


class WebCacheClient:
    def get_proxy_list(self, numproxies: int = 1000):
        """
        gets list of proxies from data service. Some of the proxies might not work, but the probability of having a
        majority of good proxies is rather high.
        :param numproxies: maximal number of proxies returned (the higher the number of proxies, the larger the share of non-working proxies. Usually you can expect there to be around 1500 working proxies in the service at any given time)
        :return: list of proxies
        """
        if numproxies < 1:
            return []
        else:
            serviceURL = "%s/proxies/%s" % (settings.WEBCACHE_LOCATION, numproxies)
            response = requests.get(serviceURL)
            if response.status_code != 200:
                raise requests.RequestException(response=response)

            data = response.json()
            if data is not None and "response" in data:
                return data["response"]
            else:
                raise ValueError("could not get proxies: %s" % data)

    def fetch_urls(self, url_list, category:str, output, method="GET", max_age_days=360):
        '''
        uses data service to fetch a list of urls

        :param url_list: non-empty list of url's to be obtained. if data is included in a POST request -> list of tuples(url, data-json)
        :param category: name of the dataprocessor issuing request and type of request. Example: "dataprocessor_geocoder:find-latlon". Only for logging purposes
        :param output: how should the cache output be interpreted and serialised? supported are JSON and XML (BeautifulSoup object returned)
        :param method: GET/POST
        :param max_age_days: maximum age of page in cache in days. If a URL has been cached longer ago than these days, it is fetched again
        :return: dictionary where input url's are mapped to cache-result. available fields in cache-result-dict: content, size, url, format, creation_date, urlKey
        '''
        if output.lower() not in ["json", "xml"]:
            raise ValueError("output-field must be either JSON or XML")

        if method.upper() not in ["GET", "POST"]:
            raise ValueError("the web cache currently only supports GET and POST Requests")

        filtered_url_list = []
        for url_item in url_list:
            url_tuple = (url_item, '{}') if type(url_item) is str else url_item

            if is_valid_url(url_tuple[0]):
                filtered_url_list.append(url_tuple)
            else:
                print("invalid URL supplied to cache: %s. will ignore it" % url_tuple[0])

        service_url = f'{settings.WEBCACHE_LOCATION}/fetch/{max_age_days}/{category}/{output}/{method}'

        # TODO: investigate it, maybe move to mockups
        if any('localhost' in url[0] or '127.0.0.1' in url[0] for url in filtered_url_list):
            data = {}
            for url in filtered_url_list:
                data[url[0]] = {'content':requests.get(url[0]).json()}
            return data

        data = requests.post(service_url, {"urls": json.dumps(filtered_url_list)}).json()

        if "error" in data:
            raise ValueError("cache could not obtain data. Error: %s" % data["error"])

        url_keys = {}
        for page_data in data["response"]:
            bz_2 = page_data.get('content_bz2')
            raw_bz_2 = page_data.get('content_raw_bz2')
            data = bz_2 or raw_bz_2
            if data:
                b64decoded = base64.b64decode(data[2:])
                decompressed = bz2.decompress(b64decoded)
                if bz_2:
                    page_data["content"] = pickle.loads(decompressed)
                else:
                    logging.warning(f'Could not parse url {normalize_url(page_data["urlTuple"])} into {output}')
                    page_data["content"] = decompressed

                if 'content_bz2' in page_data:
                    del page_data['content_bz2']
                if 'content_raw_bz2' in page_data:
                    del page_data['content_raw_bz2']

                url_keys[page_data["urlKey"]] = page_data
            else:
                logging.error("There was a problem processing URL %s" % page_data.get('urlKey'))

        return {url_item: url_keys.get(normalize_url(url_item), {"url": url_item, "content": None, "error": True})
                for url_item in url_list}


def is_valid_url(url):
    return type(url) == str and len(url.strip()) > 0 and url.startswith("http")


def normalize_url(url_item):
    url, data = (url_item, {}) if type(url_item) is str else (url_item[0], json.loads(url_item[1]))
    try:
        lower_link_furl = furl.furl(
            url.lower().strip().replace("https://", "http://"))  # consider http and https as EQUAL for the key
        lower_link_furl.path.normalize()
        lower_link_furl.query.params = sorted(
            [(par, lower_link_furl.query.params[par]) for par in lower_link_furl.query.params],
            key=lambda item: item[0])
        lower_link_furl.path = "%s/" % lower_link_furl.path if not str(lower_link_furl.path).endswith(
            "/") else lower_link_furl.path
        lower_link = lower_link_furl.url

        if data:
            data_json = json.dumps(data, sort_keys=True).lower()
            lower_link = f"{lower_link}_{data_json}"

        return lower_link
    except:
        logging.error(f'Cannot normalize URL {url}')
        return None
