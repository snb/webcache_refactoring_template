import os

WEBCACHE_LOCATION = os.getenv('WEBCACHE_LOCATION', 'http://10.5.133.201:9011')
MONGO_LOCATION = os.getenv('MONGO_LOCATION', 'mongodb://localhost:27017')
FLASK_HOST = os.getenv('FLASK_IP', '127.0.0.1')
FLASK_PORT = int(os.getenv('FLASK_PORT', 9011))
FLASK_DEBUG = bool(os.getenv('FLASK_DEBUG', True))
MAX_TIMES_FOR_URL = int(os.getenv('MAX_TIMES_FOR_URL', 20))
